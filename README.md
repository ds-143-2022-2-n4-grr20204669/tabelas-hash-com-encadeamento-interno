## Estrutura de Dados II - UFPR

## Tabelas Hash com Encadeamento Interno

Implemente operações de Busca, Inserção e Remoção para uma estrutura de dados do tipo Tabela Hash, com Encadeamento Interno para solução de colisões. As chaves armazenadas devem ser strings.
Opcional: se estiver animado, implemente também as operações de redimensionamento da tabela, com as condições apresentadas na video-adula.

Coloque a implementação em um repositório do gitlab e envie o link como resposta nessa tarefa.