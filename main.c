#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 100

struct Node{
    char *key;
    int value;
    struct Node *next;
};

struct HashTable{
    struct Node *table[TABLE_SIZE];
};

unsigned int hash(char *key) {
    unsigned int hash = 5381;
    int c;

    while ((c = *key++))
        hash = ((hash << 5) + hash) + c;

    return hash % TABLE_SIZE;
}

struct HashTable *criaHashTable() {
    struct HashTable *ht = (struct HashTable *) malloc(sizeof(struct HashTable));
    int i;
    for (i = 0; i < TABLE_SIZE; i++)
        ht->table[i] = NULL;
    return ht;
}

void insert(struct HashTable *ht, char *key, int value) {
    unsigned int index = hash(key);

    struct Node *novoNo = (struct Node *) malloc(sizeof(struct Node));
    novoNo->key = key;
    novoNo->value = value;
    novoNo->next = ht->table[index];
    ht->table[index] = novoNo;
}

int search(struct HashTable *ht, char *key) {
    unsigned int index = hash(key);

    struct Node *node = ht->table[index];
    while (node != NULL)
    {
        if (strcmp(node->key, key) == 0)
            return node->value;
        node = node->next;
    }
    return -1;
}

void delete(struct HashTable *ht, char *key) {
    unsigned int index = hash(key);

    struct Node *node = ht->table[index];
    struct Node *prev = NULL;
    while (node != NULL) {
        if (strcmp(node->key, key) == 0) {
            if (prev == NULL)
                ht->table[index] = node->next;
            else
                prev->next = node->next;
            free(node);
            return;
        }
        prev = node;
        node = node->next;
    }
}

int main(void) {
    struct HashTable *ht = criaHashTable();

    insert(ht, "Jose", 25);
    insert(ht, "Maria", 32);
    insert(ht, "Joao", 40);

    int idade = search(ht, "Maria");
    printf("Idade da Maria: %d\n", idade);

    int idade2 = search(ht, "Jose");
    printf("\nIdade do Jose: %d\n", idade2);


    delete(ht, "Joao");
    idade = search(ht, "Joao");
    if (idade == -1) {
        printf("\nIdade do Joao: %d (foi deletado da Hash Table).\n", idade);
    }
    return 0;
}
